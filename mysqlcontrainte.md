# CONSTRAINTS ON MySQL

## different types of Constraints :

### PRIMARY KEY

```sql
CREATE TABLE students (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100)
  );
```

The students table is created with an id column designated as the primary key. This means that every value in the id column must be unique and not null, ensuring that each record in the table can be uniquely identified.

### UNIQUE

```sql
CREATE TABLE users (
  id INT AUTO_INCREMENT PRIMARY KEY,
  email VARCHAR(255) UNIQUE
);
```

In the users table, the email column has a UNIQUE constraint. This ensures that no duplicate emails can be stored in the table, maintaining the uniqueness of each email.

### NOT NULL

```sql
CREATE TABLE orders (
  id INT AUTO_INCREMENT PRIMARY KEY,
  orderDate DATE NOT NULL
);
```

The NOT NULL constraint on the orderDate column of the orders table prevents inserting null values for this column, ensuring that each order has a valid date.

### CHECK

```sql
CREATE TABLE products (
  id INT AUTO_INCREMENT PRIMARY KEY,
  price DECIMAL CHECK (price > 0)
);
```

The products table uses a CHECK constraint to ensure that the price of each product is greater than 0. This constraint ensures that invalid prices cannot be stored in the table.

### DEFAULT

```sql
CREATE TABLE articles (
  id INT AUTO_INCREMENT PRIMARY KEY, 
  available BOOLEAN DEFAULT TRUE
  );
```

For the available column in the articles table, the DEFAULT constraint sets the default value to TRUE. This means if no value is specified when inserting a record, the column will automatically be set to TRUE.


### FOREIGN KEY

```sql
CREATE TABLE Customers (
  CustomerID INT AUTO_INCREMENT PRIMARY KEY,
  Name varchar(255) NOT NULL,
  Email varchar(255)
);

CREATE TABLE Orders (
  OrderID INT AUTO_INCREMENT PRIMARY KEY,
  CustomerID int,
  Amount decimal(10, 2),
  OrderDate date,
  FOREIGN KEY (CustomerID) REFERENCES Customers(CustomerID)
);
```

The relationship established by the foreign key ensures each order is associated with a valid customer. If you try to insert an order with a CustomerID that doesn't exist in the Customers table, the database system rejects the operation. This prevents orphaned data and ensures the relationships between tables remain consistent.

### Composite Key

```sql
CREATE TABLE enrollments (
  studentId INT AUTO_INCREMENT PRIMARY KEY,
  courseId INT,
  PRIMARY KEY(studentId, courseId),
  FOREIGN KEY (studentId) REFERENCES students(id),
  FOREIGN KEY (courseId) REFERENCES courses(id)
);
```

The enrollments table uses a composite key as its primary key, made up of the studentId and courseId columns. This means the combination of studentId and courseId values must be unique for each record, allowing for a unique representation of a student's enrollment in a course. The foreign key constraints also ensure that the values of studentId and courseId correspond to valid records in the students and courses tables, respectively.

## Sources

[mysql doc](https://dev.mysql.com/doc/refman/8.0/en/create-table.html)

[apcpedagogie](https://apcpedagogie.com/les-contraintes-dintegrite-avec-mysql/)

[w3schools](https://www.w3schools.com/mysql/mysql_constraints.asp)

